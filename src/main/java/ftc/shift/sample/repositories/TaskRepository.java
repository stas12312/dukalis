package ftc.shift.sample.repositories;

import ftc.shift.sample.models.Task;
import ftc.shift.sample.models.enums.TaskStatusEnum;
import ftc.shift.sample.models.enums.TaskTypeEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> {
    List <Task> findByExecutor_id(Long executorId);
    List <Task> findByEmployer_idAndStatusNot(Long employerId, TaskStatusEnum status);

    //Получение списка публичных задач по статусу (Задачи выставленные пользователем игнорируются)
    List<Task> findByStatusAndEmployer_IdNot(TaskStatusEnum status, Long employerId);

    //Получение всех задач выставленных пользователем
    List<Task> findByEmployer_Id(Long id, Sort sort);

    // Получение всех задач выставленных не пользователям
    List<Task> findByEmployer_IdNot(Long id);

    // Запрос для получения выставленных пользователем задач по статусу с пагинацией
    @Query("SELECT t FROM Task t WHERE t.employer.id = :id AND t.status = :status")
    Page<Task> getPersonalTasksByStatus(
            @Param("id") Long id,
            @Param("status") TaskStatusEnum status,
            Pageable pageable);

    // Запрос на получение выполняемых задач по статусу
    @Query("SELECT t FROM Task t WHERE t.executor.id = :id AND t.status = :status")
    Page<Task> getPublicTasksByStatus(
            @Param("id") Long id,
            @Param("status") TaskStatusEnum status,
            Pageable pageable);

    // Запрос на получение доступных для выполнения задач
    @Query("SELECT t FROM Task t WHERE  NOT t.employer.id = :id AND t.status = 0")
    Page<Task> getPublicAvailableTasks(
            @Param("id") Long id,
            Pageable pageable);

    // Запрос на получение доступных для выполнения задач
    @Query("SELECT t FROM Task t WHERE  NOT t.employer.id = :id AND t.status = 0 AND t.type = :category ")
    Page<Task> getPublicAvailableTasksByCategory(
            @Param("id") Long id,
            @Param("category") TaskTypeEnum category,
            Pageable pageable);
}
