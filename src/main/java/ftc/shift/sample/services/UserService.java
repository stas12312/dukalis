package ftc.shift.sample.services;

import ftc.shift.sample.exception.PermissionDeniedException;
import ftc.shift.sample.exception.UserNotFoundException;
import ftc.shift.sample.models.User;
import ftc.shift.sample.models.enums.UserRoleEnum;
import ftc.shift.sample.models.enums.UserTypeEnum;
import ftc.shift.sample.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepo;

    public UserService(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    public User getUserById(Long userId) {
        /*
        Получение пользователя по ID
        Если пользователя не существует, возвращает ошибку
        */
        return userRepo.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
    }


    public User createUser(User user) {
        /*
        Создание пользователя
        Карма и ID заполняются по умолчанию
         */
        if (userRepo.getUserByUsername(user.getUsername()) != null) {
            throw new PermissionDeniedException();
        }
        user.setDukalises(500); // Начисляем пользователю 500 дукалисов
        user.setKarma(0); // Карму устанавливаем в 0
        user.setFreePersonalTasks(0);
        user.setType(UserTypeEnum.PASSIVE); // Тип пользователя пассивный
        user.setRoles(UserRoleEnum.USER); // Роль пользователя
        return userRepo.save(user);
    }

    public List<User> getAllUsers() {
        /*
        Получение всех пользователей из БД
        */
        return userRepo.findAll();
    }

    public User authUser(String username) {
        /*
        Получение пользователя по username
         */
        if (userRepo.getUserByUsername(username) == null) {
            throw new UserNotFoundException(null);
        }
        return userRepo.getUserByUsername(username);
    }

    public User updateUser(User user) {
        return userRepo.save(user);
    }


    /**
     * Метод для выбора лучшего пользователя по количеству Кармы
     */
    public void setBestUser() {
        // Получаем старых лучших пользователей
        List<User> bestUsers = userRepo.getBestUsers();

        //Получаем значение максимальной кармы
        Integer maxKarma = userRepo.getMaxKarma();

        // Получаем новых пользователей с максимальной кармой
        List<User> newBestUsers = userRepo.findByKarma(maxKarma);

        // Убираем статус ЛУЧШИЙ у старого списка
        bestUsers.stream()
                .peek(user -> user.setRoles(UserRoleEnum.USER))
                .forEach(userRepo::save);

        // Присваеваем статус лучший новому списку
        newBestUsers.stream()
                .peek(user -> user.setRoles(UserRoleEnum.BEST_USER))
                .forEach(userRepo::save);


        // Установка двух бесплатных личных просьб для лучших
        newBestUsers.stream()
                .peek(user -> user.setFreePersonalTasks(2))
                .forEach(userRepo::save);
    }


    public void resetKarmaForAllUsers() {

        // Уменьшаем значение кармы на 60
        userRepo.resetKarma();
    }

}
