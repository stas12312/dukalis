package ftc.shift.sample.models.enums;

/**
 * Тип операции
 */
public enum TransactionTypeEnum {
    CHARGE, // Начисление
    PAYMENT, // Оплата
    FINE   // Штраф
}
