package ftc.shift.sample.models.enums;

public enum UserRoleEnum {
    ADMIN,      // Администратор
    USER,       // Пользователь
    BEST_USER;  // Лучший пользователь

    UserRoleEnum() {

    }
}
