package ftc.shift.sample.models.enums;

public enum TaskTypeEnum {
    PRIVATE, // Личная
    SOCIAL;  // Социальная
}
