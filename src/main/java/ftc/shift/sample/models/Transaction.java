package ftc.shift.sample.models;

import ftc.shift.sample.models.enums.TransactionTypeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="TRANSACTIONS")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "Уникальный идентификатор транзакции")
    private Long id;

    @ApiModelProperty(value = "Тип операции")
    private TransactionTypeEnum type;

    @ApiModelProperty(value = "Пользователь")
    @ManyToOne()
    private User user;

    @CreationTimestamp
    @ColumnDefault("CURRENT_TIMESTAMP")
    @ApiModelProperty(value = "Время операции")
    private Date dateOperation;

    @ApiModelProperty(value = "Сумма")
    private Integer amount;

    @ApiModelProperty(value = "Комментарий")
    private String comment;

    public Transaction(){

    }

    public Transaction(TransactionTypeEnum type, Date dateOperation, Integer amount, String comment, User user) {
        this.type = type;
        this.dateOperation = dateOperation;
        this.amount = amount;
        this.comment = comment;
        this.user = user;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransactionTypeEnum getType() {
        return type;
    }

    public void setType(TransactionTypeEnum type) {
        this.type = type;
    }

    public Date getDateOperation() {
        return dateOperation;
    }

    public void setDateOperation(Date dateOperation) {
        this.dateOperation = dateOperation;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
