package ftc.shift.sample.models;

import ftc.shift.sample.models.enums.UserRoleEnum;
import ftc.shift.sample.models.enums.UserTypeEnum;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Entity
@Table(name="USERS")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "Уникальный идентификатор пользователя", required = true)
    private Long id;

    @NotEmpty(message = "Provide a username")
    @ApiModelProperty(value = "Псевдоним пользователя", required = true)
    private String username;

    @NotEmpty(message = "Provide a first name")
    @ApiModelProperty(value = "Имя пользователя", required = true)
    private String firstName;

    @NotEmpty(message = "Provide a last name")
    @ApiModelProperty(value = "Фамилия пользователя", required = true)
    private String lastName;

    @NotEmpty(message = "Provide a email")
    @ApiModelProperty(value = "E-mail пользователя", required = true)
    private String email;

    @Min(value = 14, message = "The minimum age for registration is 14 years")
    @ApiModelProperty(value = "Возраст пользователя", required = true)
    private Integer age;

    @NotEmpty(message = "Provide a city")
    @ApiModelProperty(value = "Город пользователя", required = true)
    private String city;

    @ApiModelProperty(value = "Количество кармы у пользователя", required = true)
    private Integer karma;

    @ApiModelProperty(value = "Статус пользователя", required = true)
    private UserTypeEnum type;

    @ApiModelProperty(value = "Роль пользователя", required = true)
    private UserRoleEnum roles;


    @ApiModelProperty(value = "Контакты пользователя", required = true)
    private String contacts;

    @ApiModelProperty(value = "Количество дукалисов", required = false)
    private Integer dukalises;

    @ApiModelProperty(value = "Количество бесплатных личных заданий", required = false)
    private Integer freePersonalTasks;

    public User(String username, String firstName, String lastName,
                String email, Integer age, String city, Integer karma,
                UserTypeEnum type, UserRoleEnum roles, String contacts,
                Integer dukalises, Integer freePersonalTasks) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.city = city;
        this.karma = karma;
        this.type = type;
        this.roles = roles;
        this.contacts = contacts;
        this.dukalises = dukalises;
        this.freePersonalTasks = freePersonalTasks;
    }




    public User() {

    }

    /**
     * Метод добавляет пользователю карму
     * @param karma Количество кармы, добавляемое пользователю
     * @return Общее кол-во кармы у пользователей
     */
    public Integer addKarma(Integer karma) {
        this.karma += karma;
        return this.karma;
    }

    /**
     * Метод добавляет пользователю дукалисы
     * @param dukalises Количество дукалисов, добавляемое пользователю
     * @return Общее кол-во дукалисов у пользователя
     */
    public Integer addDukalises(Integer dukalises) {
        this.dukalises += dukalises;
        return this.dukalises;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getKarma() {
        return karma;
    }

    public void setKarma(Integer karma) {
        this.karma = karma;
    }

    public UserTypeEnum getType() {
        return type;
    }

    public void setType(UserTypeEnum type) {
        this.type = type;
    }

    public UserRoleEnum getRoles() {
        return roles;
    }

    public void setRoles (UserRoleEnum roles) {
        this.roles = roles;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public Integer getDukalises() {
        return dukalises;
    }

    public void setDukalises(Integer dukalises) {
        this.dukalises = dukalises;
    }

    public Integer getFreePersonalTasks() {
        return freePersonalTasks;
    }

    public void setFreePersonalTasks(Integer freePersonalTasks) {
        this.freePersonalTasks = freePersonalTasks;
    }

    public Integer decrementFreePersonalTasks() {
        int freePersonalTasks = getFreePersonalTasks();
        freePersonalTasks -= 1;
        if (freePersonalTasks < 0) freePersonalTasks = 0;
        setFreePersonalTasks(freePersonalTasks);
        return getFreePersonalTasks();
    }
}
