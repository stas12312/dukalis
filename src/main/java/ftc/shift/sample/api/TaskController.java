package ftc.shift.sample.api;

import ftc.shift.sample.exception.NotFoundException;
import ftc.shift.sample.models.Task;
import ftc.shift.sample.models.enums.TaskStatusEnum;
import ftc.shift.sample.models.enums.TaskTypeEnum;
import ftc.shift.sample.repositories.TaskRepository;
import ftc.shift.sample.services.TaskService;
import io.swagger.annotations.*;
import org.hibernate.annotations.NotFound;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/v001/tasks")
@Api(tags = {"Запросы для работы с задачами"})
public class TaskController {

    private final TaskRepository taskRepo;
    private final TaskService service;

    public TaskController(TaskRepository taskRepo, TaskService service) {
        this.taskRepo = taskRepo;
        this.service = service;
    }

    @GetMapping()
    @ApiOperation(value = "Получение списка заданий")
    public List<Task> listTasks(
            @ApiParam(
                    value = "Статус задачи",
                    allowableValues = "ACTIVE, PROGRESS, DONE"
                    )
            @RequestParam(value = "status", required = false) TaskStatusEnum status,
            @ApiParam(
                    value = "Тип задачи",
                    allowableValues = "PUBLIC, PERSONAL",
                    defaultValue = "PUBLIC")
            @RequestParam(value = "type",required = false, defaultValue = "PUBLIC") String type,
            @ApiParam(
                    value = "Категория задачи (Только для PUBLIC со статусом ACTIVE)",
                    allowableValues = "PRIVATE, SOCIAL"
            )
            @RequestParam(value = "category", required = false) TaskTypeEnum category,
            @ApiParam(value = "Координаты человека")
            @RequestParam(value = "peopleCoordinate", required = false) String peopleCoordinate,
            @ApiParam(value = "Идентификатор пользователя", required = true)
            @RequestHeader("userId") Long userId,
            @ApiParam(value = "Номер страницы", defaultValue = "0")
            @RequestParam(value="page", required = false, defaultValue = "0") int page,
            @ApiParam(value = "Количество задач", defaultValue = "20")
            @RequestParam(value = "count", required = false, defaultValue = "20") int count
    ) {
        return service.getTasks(status, userId, type, category, peopleCoordinate, page, count);
    }

    @PostMapping
    @ApiOperation(value = "Добавление задания")
    public Task createTask(
            @ApiParam(value = "Данные для нового задания", required = true)
            @Valid @RequestBody Task task,
            @ApiParam(value = "Идентификатор пользователя", required = true)
            @RequestHeader("userId") Long userId
    ) {
        return service.createTask(task, userId);
    }

    @GetMapping("{taskId}/apply")
    @ApiOperation(value = "Принятие задания пользователем")
    public Task applyTask(
            @ApiParam(value="Идентификатор пользователя", required = true)
            @RequestHeader("userId") Long userId,
            @ApiParam(value="Идентификатор задачи", required = true)
            @PathVariable Long taskId
    ){
        return service.applyTask(taskId, userId);
    }

    @GetMapping("{taskId}/complete")
    @ApiOperation(value="Завершение заказчиком задания")
    public Task completeTask(
        @ApiParam(value="Идентификатор пользователя", required = true)
        @RequestHeader("userId") Long userId,
        @ApiParam(value="Идентификатор задачи", required = true)
        @PathVariable Long taskId
    ){
        return service.completeTask(taskId, userId);
    }

    @GetMapping("{taskId}/cancel")
    @ApiOperation(value="Отказ исполнителя от задания")
    public Task cancelTask(
        @ApiParam(value="Идентификатор пользователя", required = true)
        @RequestHeader("userId") Long userId,
        @ApiParam(value="Идентификатор задачи", required = true)
        @PathVariable Long taskId
    ){
        return service.cancelTask(taskId, userId);
    }

    @GetMapping("{taskId}/complain")
    @ApiOperation(value="Жалоба заказчика на исполнителя")
    public Task complainTask(
            @ApiParam(value="Идентификатор пользователя", required = true)
            @RequestHeader("userId") Long userId,
            @ApiParam(value="Идентификатор задач", required = true)
            @PathVariable Long taskId
    ){
        return service.complainTask(userId, taskId);
    }

    @GetMapping("{taskId}")
    @ApiOperation(value = "Получение задачи по идентификатору")
    public Task getTask(
            @ApiParam(value = "Идентификатор пользователя", required = true)
            @RequestHeader("userId") Long userId,
            @ApiParam(value = "Идентификатор задачи")
            @PathVariable Long taskId
    ){
        return service.getTask(userId, taskId);
    }

}
