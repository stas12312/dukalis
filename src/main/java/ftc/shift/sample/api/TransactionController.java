package ftc.shift.sample.api;

import ftc.shift.sample.models.Transaction;
import ftc.shift.sample.services.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/v001/transactions")
@Api(tags = {"Запросы для работы с транзакциями"})
public class TransactionController {

    private final TransactionService service;

    @Autowired
    public TransactionController(TransactionService service) {
        this.service = service;
    }

    @GetMapping()
    @ApiOperation(value = "Получение всех транзакций пользователя")
    public ResponseEntity<List<Transaction>> getTransaction(
            @ApiParam(value = "Идентификатор пользователя", required = true)
            @RequestHeader Long userId) {
        List<Transaction> transactions = service.getTransactionsForUser(userId);
        return ResponseEntity.ok(transactions);
    }


}
