package ftc.shift.sample.utils.sorting;

public class PointMap {
    private float x;
    private float y;

    public PointMap(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public static PointMap convertString(String coord) {
        if (coord == null)
            return null;
        String[] numbers = coord.split("[, ]");
        return new PointMap(Float.parseFloat(numbers[0]), Float.parseFloat(numbers[1]));
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }
}
