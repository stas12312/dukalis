package ftc.shift.sample.utils.sorting;

import ftc.shift.sample.models.Task;

import java.util.List;

public interface SortBy {
    public List<Task> sorting();
}
